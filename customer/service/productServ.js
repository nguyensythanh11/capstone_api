const BASE_URL = "https://647f22b5c246f166da90250e.mockapi.io/camera";

let productServ = {
    getList: () => { 
        return axios({
            url: BASE_URL,
            method: "GET",
        })
    },
    getProduct: (id) => { 
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET",
        });
    },
};

export default productServ;