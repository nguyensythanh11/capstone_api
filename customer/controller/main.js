import productServ from "../service/productServ.js"
import { getLocalStorage, renderCartList, renderProductList, saveLocalStorage, sweetAlert } from "./controller.js";

let cart = [];

cart = getLocalStorage();

let fetchProductList = () => { 
    productServ.getList()
        .then((res) => {
            renderProductList(res.data);
          })
          .catch((err) => {
           console.log(err);
          });
 }
fetchProductList();

let showProductListWithCondition = () => { 
    fetchProductList();
}
window.showProductListWithCondition = showProductListWithCondition;

let showInformationToCart = () => {
    renderCartList(cart);
}
showInformationToCart();

let getDetailProduct =(id) => { 
    productServ.getProduct(id)
        .then((res) => {
                if(cart == null){
                    res.data.quantity = 1;
                    cart.push(res.data);
                }
                else{
                    let index = cart.findIndex((cartProduct) => {
                        return cartProduct.id == id;
                    })
                    if(index == -1){
                        res.data.quantity = 1;
                        cart.push(res.data);
                    }
                    else{
                        cart[index].quantity++;
                    }
                }
                renderCartList(cart);
                saveLocalStorage(cart);
              })
              .catch((err) => {
               console.log(err);
              });
}
window.getDetailProduct = getDetailProduct;

let subtractButton = (id) => {
    let index = cart.findIndex((cartProduct) => {
        return cartProduct.id == id;
    })
    if(index != -1){
        cart[index].quantity --;
    }
    if(cart[index].quantity == 0) cart.splice(index,1);
    renderCartList(cart);
    saveLocalStorage(cart);
}
window.subtractButton = subtractButton;

let plusButton = (id) => {
    let index = cart.findIndex((cartProduct) => {
        return cartProduct.id == id;
    })
    cart[index].quantity ++;
    renderCartList(cart);
    saveLocalStorage(cart);
}
window.plusButton = plusButton;

let removeButton = (id) => {
    let index = cart.findIndex((cartProduct) => {
        return cartProduct.id == id;
    });
    cart[index].quantity = 0;
    cart.splice(index,1);
    renderCartList(cart);
    saveLocalStorage(cart);
}
window.removeButton = removeButton;

let emptyCartButton = () => { 
    cart = [];
    renderCartList(cart);
    saveLocalStorage(cart);
 }
window.emptyCartButton = emptyCartButton;

let payButton = () => { 
    if(cart.length == 0){
        sweetAlert("error","Your cart is empty");
    }   
    else{
        sweetAlert("success","Your order is completed");
    }
    emptyCartButton();
}
window.payButton = payButton;

