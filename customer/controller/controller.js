const IPHONE = true;
const SAMSUNG = false;

export let renderProductList = (productArray) => { 
    let contentHTML = "";
    productArray.forEach((product) => { 
        let {
            name,
            price,
            screen,
            backCamera,
            frontCamera,
            img,
            desc,
            type,
            id
        } = product;
        let checkBrand = document.getElementById("brand").value;
        console.log("🚀 ~ file: controller.js:19 ~ productArray.forEach ~ checkBrand:", checkBrand)
        let content =
        `
        <div class="item bg-white shadow-xl rounded-2xl">
        <div class="wrapper">
            <img class="w-full min-h-full" src="${img}" alt="Phone Image">
        </div>
        <div class="desc">
            <h3 class="font-semibold text-center mb-2">${name}</h3>
            <h4 class="text-center mb-2">${price}</h4>
            <span class="bg-gray-400 text-center ml-6">${type == true ? "Iphone" : "Samsung"}</span>
            <br> <br>
            <span class="font-semibold ml-6 ">Description:</span> ${desc}
            <div class="evaluate flex justify-between mt-2 pb-3">
                <div class="left ml-6 text-yellow-300">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
                <div class="right mr-6">
                    <p class="text-green-500 font-medium">In Stock</p>
                </div>
            </div> 
        </div>
        <div class="add">
            <button onclick = "getDetailProduct('${id}')" class="ml-3 p-2 mb-3 font-medium rounded-lg  bg-yellow-500 hover:bg-yellow-400 transition ease-in-out delay-150  hover:-translate-y-1 hover:scale-110  duration-300">Add To Cart</button>
        </div>
    </div>
        `
        if(checkBrand == 1){
            if(type == IPHONE){
               contentHTML += content;
            }
        }
        else if(checkBrand == 2){
            if(type == SAMSUNG){
               contentHTML += content;
            }
        } else {
            contentHTML += content;
        }
     });
    document.getElementById("listDetail").innerHTML = contentHTML;
}

export let saveLocalStorage = (productList) => { 
    let dataJson = JSON.stringify(productList);
    localStorage.setItem("DSSP",dataJson);
}

export let getLocalStorage = () => { 
    let dataJson = localStorage.getItem("DSSP");
    if(dataJson != null){
        let productList = JSON.parse(dataJson);
        productList.map((product) => { 
            return {
                ... product,
            }
        })
        return productList;
    }
    return [];
}

export let renderCartList = (cartList) => {
    if(cartList == null){
        document.getElementById("subtotal").innerHTML = 0;
        document.getElementById("shipping").innerText = 0;
        document.getElementById("tax").innerText = 0;
        document.getElementById("totalQuantity").innerText = 0;
        return;
    }
    let contentHTML = "";
  
    let subTotal = 0;
    let ship = 0;
    let tax = 0;
    let totalQuantity = 0;
    cartList.forEach((cartProduct) => { 
        let {
            name,
            price,
            screen,
            backCamera,
            frontCamera,
            img,
            desc,
            type,
            id,
            quantity
        } = cartProduct;
        subTotal += price*quantity;
        ship += 10*quantity;
        tax += 200*quantity;
        totalQuantity += quantity;

        let content = `
        <div class="top flex justify-start ">
            <div class="left">
                <img class="w-60" src="${img}" alt="Italian Trulli">
            </div>
            <div class="right pt-4 space-y-2">
                <h3 class="font-medium">${name}</h3>
                <p>Screen: ${screen}</p>
                <p>Back Camera: ${backCamera}</p>
                <p>Front Camera: ${frontCamera}</p>
                <button onclick = "removeButton('${id}')">Remove</button>
            </div>
        </div>
        <div class="bottom flex justify-between px-4 mr-8 mb-3 mt-2">
            <div class="left">
                <span>Quantity:</span>
                <button onclick = "subtractButton('${id}')" class="round-1 border rounded-full">-</button>
                <span class="mx-2">${quantity}</span>
                <button onclick = "plusButton('${id}')" class="round-1 border rounded-full">+</button>
            </div>
            <div class="right">$
                ${price}
            </div>
        </div>
        `;
        contentHTML += content;
     })
    document.getElementById("subtotal").innerHTML = subTotal;
    document.getElementById("shipping").innerText = ship;
    document.getElementById("tax").innerText = tax;
    document.getElementById("total").innerText = subTotal + ship + tax;  
    document.getElementById("totalQuantity").innerText = totalQuantity;

    document.getElementById("product-order").innerHTML = contentHTML;
}

export let sweetAlert = (icon, title) => { 
    Swal.fire({
        position: 'center-center',
        icon,
        title,
        showConfirmButton: false,
        timer: 1500
    })
}


