import productServ from "../service/productServ.js"
import { getDetailFromForm, renderProductList, showDetailToForm, sweetAlert} from "./controller.js";
import checkValid from "./validation.js";

let idUpdate;
let productList = [];

let fecthProductList = () => { 
  productServ.getList()
    .then((res) => {
          console.log(res);
          renderProductList(res.data.reverse());
          productList = [... res.data];
        })
        .catch((err) => {
          console.log(err);
        });
}
fecthProductList();

let createNewProduct = () => { 
  let isValid = checkValid(getDetailFromForm());
  if(isValid){
    productServ.createProduct(getDetailFromForm())
    .then((res) => {
            console.log(res);           
            fecthProductList();
            $('#myModal').modal('hide');
            resetSP();
            sweetAlert("success","Thêm thành công!")
          })
          .catch((err) => {
           console.log(err);
           sweetAlert("error","Thêm thất bại!")
          }); 
  }
}
window.createNewProduct = createNewProduct;

let closeButton = () => { 
  $('#myModal').modal('hide');
  resetSP();
}
window.closeButton = closeButton;

let deleteSP = (id) => { 
  productServ.deleteProduct(id)
    .then((res) => {
            console.log(res);
            fecthProductList();
            sweetAlert("success","Xóa thành công")
          })
          .catch((err) => {
           console.log(err);
          });
 }
window.deleteSP = deleteSP;

let suaSP = (id) => {
  idUpdate = id;
  productServ.getProduct(id)
    .then((res) => {
            console.log(res);
            showDetailToForm(res.data);
            $('#myModal').modal('show');
          })
          .catch((err) => {
           console.log(err);
          });
}
window.suaSP = suaSP;

let updateSP = () => { 
  let isValid = checkValid(getDetailFromForm());
  if(isValid){
    productServ.updateProduct(getDetailFromForm(),idUpdate)
    .then((res) => {
            console.log(res);
            fecthProductList();
            $('#myModal').modal('hide');
            resetSP();
            sweetAlert("success","Update thành công")
          })
          .catch((err) => {
           console.log(err);
          });  
  }
}
window.updateSP = updateSP;

let resetSP = () => {
  document.querySelector("#myForm").reset();
}
window.resetSP = resetSP;

let searchPhoneName = () => {
  console.log(productList);
  let nameSP = document.getElementById("searchSP").value;
  let newProductList = [];
  productList.forEach((product) => { 
    let {
      name,
      price,
      screen,
      backCamera,
      frontCamera,
      img,
      desc,
      type,
      id
    } = product;
    if(name == nameSP){
      newProductList.push(product);
    }
  })
  renderProductList(newProductList);
}
window.searchPhoneName = searchPhoneName;

let sortPrice = () => {
  let newProductList = [... productList];
  console.log(newProductList[0]);
  console.log(newProductList[1]);

  for(let i=0; i<newProductList.length-1; i++){
    for(let j = i+1; j<newProductList.length; j++){
      let price_1 = newProductList[i].price;
      let price_2 = newProductList[j].price;
      if(price_1 < price_2){
        let temp = newProductList[i];
        newProductList[i] = newProductList[j];
        newProductList[j] = temp;
      }
    }
  }
  renderProductList(newProductList);
}
window.sortPrice = sortPrice;