export let renderProductList = (productArray) => { 
    let contentHTML = "";
    productArray.forEach((product) => { 
        let {
            name,
            price,
            screen,
            backCamera,
            frontCamera,
            img,
            desc,
            type,
            id
        } = product;
        let content = `
        <tr>
            <td>${id}</td>
            <td>${name}</td>
            <td>${price}</td>
            <td>${img}</td>
            <td>${desc}</td>
            <td>
                <button onclick = "deleteSP('${id}')" class="btn btn-danger">Xóa</button>
                <button onclick = "suaSP('${id}')" class="btn btn-warning">Sửa</button>
            </td>
        </tr>
        `;
        contentHTML += content;
    })
    document.getElementById("danhSachSP").innerHTML = contentHTML;
}

export let getDetailFromForm = () => { 
    let name = document.getElementById("nameSP").value;
    let price = document.getElementById("priceSP").value*1;
    let screen = document.getElementById("screenSP").value;
    let backCamera = document.getElementById("backCameraSP").value;
    let frontCamera = document.getElementById("frontCameraSP").value;
    let img = document.getElementById("imageSP").value;
    let desc = document.getElementById("descSP").value;
    let type = document.getElementById("brandSP").value;
    return {
        name,
        price,
        screen,
        backCamera,
        frontCamera,
        img,
        desc,
        type,
    }
}

export let showDetailToForm = (product) => { 
    let {name,
        price,
        screen,
        backCamera,
        frontCamera,
        img,
        desc,
        type} = product;
    document.getElementById("nameSP").value = name;
    document.getElementById("priceSP").value = price;
    document.getElementById("screenSP").value = screen;
    document.getElementById("backCameraSP").value = backCamera;
    document.getElementById("frontCameraSP").value = frontCamera;
    document.getElementById("imageSP").value = img;
    document.getElementById("descSP").value = desc;
    document.getElementById("brandSP").value = type == true ? "Iphone" : "Samsung";   
}

export let sweetAlert = (icon, title) => { 
    Swal.fire({
        position: 'center-center',
        icon,
        title,
        showConfirmButton: false,
        timer: 1500
    })
 }
