let showMessage = (spanId,message) => {
    document.getElementById(spanId).style.display = "block";
    document.getElementById(spanId).innerHTML = message;
}

let checkName = (product) => {
    let length = product.name;
    if(length == 0){
        showMessage("nameSPVal","Please enter your phone's name");
        return false;
    }
    const re = /[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễếệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/u;
    if(re.test(product.name)){
        showMessage("nameSPVal","");
        return true;
    }
    showMessage("nameSPVal","Phone's name consists only of letters");
    return false;
}

let checkPrice = (product) => { 
    if(product.price == 0){
        showMessage("priceSPVal","Enter your phone's price");
        return false;
    }
    const re = /^(?:-(?:[1-9](?:\d{0,2}(?:,\d{3})+|\d*))|(?:0|(?:[1-9](?:\d{0,2}(?:,\d{3})+|\d*))))(?:.\d+|)$/;
    if(re.test(product.price)){
        showMessage("priceSPVal","");
        return true;
    }
    showMessage("priceSPVal","Phone's name consists only of numbers");
    return false;
}

let checkScreen = (product) => {
    if(product.screen.length == 0){
        showMessage("screenSPVal","Please enter your screen's phone");
        return false;
    }
    showMessage("screenSPVal","");
    return true;
}

let checkBackCamera = (product) => {
    if(product.backCamera.length == 0){
        showMessage("backCameraSPVal","Please enter name of your back camera phone");
        return false;
    }
    showMessage("backCameraSPVal","");
    return true;
}

let checkFrontCamera = (product) => {
    if(product.frontCamera.length == 0){
        showMessage("frontCameraSPVal","Please enter name of your front camera phone");
        return false;
    }
    showMessage("frontCameraSPVal","");
    return true;
}

let checkImageLink = (product) => {
    if(product.img.length == 0){
        showMessage("imageSPVal","Please enter link of your phone's image");
        return false;
    }
    const re = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/
    if(re.test(product.img)){
        showMessage("imageSPVal","");
        return true;
    }
    showMessage("imageSPVal","Please enter valid fomat of your phone's image");
    return false;
}

let checkDescription = (product) => {
    if(product.desc.length == 0){
        showMessage("descSPVal","Please enter your phone's description");
        return false;
    }
    showMessage("descSPVal","");
    return true;
}

let checkBrand = (product) => {
    if(product.type == 0){
        showMessage("brandSPVal","Please enter your brand's description");
        return false;
    }
    showMessage("brandSPVal","");
    return true;
}

let checkValid = (product) => {
    let isValid = checkName(product);
    isValid = isValid & checkPrice(product);
    isValid = isValid & checkScreen(product);
    isValid = isValid & checkBackCamera(product);
    isValid = isValid & checkFrontCamera(product);
    isValid = isValid & checkImageLink(product);
    isValid = isValid & checkDescription(product);
    isValid = isValid & checkBrand(product);
    return isValid;
}

export default checkValid;