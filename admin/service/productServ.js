const BASE_URL = "https://647f22b5c246f166da90250e.mockapi.io/camera";

let productServ = {
    getList: () => { 
        return axios({
            url: BASE_URL,
            method: "GET",
        });
    },
    createProduct: (product) => { 
        return axios({
            url: BASE_URL,
            method: "POST",
            data: product,
        })
    },
    deleteProduct: (id) => { 
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE"
        })
    },
    getProduct: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET"
        })
    },
    updateProduct: (product,id)=> {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "PuT",
            data: product,
        });
    }
}

export default productServ;